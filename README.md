# Program Chat Sederhana Menggunakan Protokol MQTT (Publish - Subscribe)

Message Queuing Telemetry Transport atau kepanjangan dari MQTT merupakan protokol konektivitas machine-to-machine (M2M)/ Internet of Things (IoT) berbasiskan open source.
MQTT menggunakan standar (OASIS) yang dirancang untuk perangkat dengan resource terbatas dan bandwidth rendah, serta latency tinggi. 
MQTT sangat ideal untuk perangkat yang terhubung dan aplikasi mobile di era M2M/IoT dimana bandwidth dan daya baterai menjadi pertimbangan utama.

## Getting Started

### Prerequisites

Yang dibutuhkan untuk membuat project ini dan cara menginstallnya : 

* Sistem Operasi
1.  Ubuntu Versi 18.04

* Library
1.  Python
2.  PIP
3.  Library Paho Mqtt
4.  Library Tkinter

### Installasi Prerequisites

Berikut adalah cara cara menginstall prerequisites diatas

python digunakan untuk menjalankan program chat sederhana. 

berikut cara untuk menginstall Python pada Ubuntu

untuk Python 2
>$ sudo apt-get install python 

atau untuk Python 3
>$ sudo apt-get install python3


PIP adalah singkatan dari **P**IP **I**nstall **P**ython atau **P**IP **I**nstall **P**ackages,
pip digunakan untuk menginstall library yang akan digunakan pada project ini 


berikut cara untuk penginstall PIP pada ubuntu

untuk install pip pada python 2
>$ sudo apt-get install python-pip

untuk install pip pada python 3
>$ sudo apt-get install python3-pip


Paho Mqtt digunakan untuk menjalankan protokol mqtt pada python. 

berikut cara untuk menginstall Paho Mqtt pada Ubuntu 18.04

>$ sudo pip install paho-mqtt

Library Tkinter digunakan untuk membuat tampilan user interface program chat sederhana. 

berikut cara untuk menginstall Tkinter pada Ubuntu 18.04

untuk install Tkinter pada python 2
>$ sudo apt-get install python-tk

untuk install Tkinter pada python 3
>$ sudo apt-get install python3-tk


## Menjalankan Program

* jalankan file simplechatMQTT.py untuk menjalankan program chat sederhana
* inputkan username sebagai data untuk tanda publish
* inputkan username teman yang akan melakukan proses chat, dan pilih connect
* proses chat dapat dilakukan ketika dua mesin telah terhubung


## Authors

* **Anjar Afrizal** - 1301164459 - *Initial work* - [@anjarafrizal12](https://gitlab.com/anjarafrizal12)
* **Alan Maulana Ibrahim** - 1301154175 - *Initial work* - [@alannmaulanaa](https://gitlab.com/alannmaulanaa)
* **Mia Amelia** - 1301164310 - *Initial work* - [@Miaamelia](https://gitlab.com/Miaamelia)
* **Muhammad Hadiyan Wicaksono** - 1301154220 - *Initial work* - [@hadicaksono](https://gitlab.com/hadicaksono)
* **Ramawaldi Putra** - 1301164506 - *Initial work* - [@ramawaldiputra](https://gitlab.com/ramawaldiputra)